<?php
include 'partials/header.php';
?>
<div class="hero4">
  <div class="transparent_layer">
    <p class="font1">Lorem ipsum sometimes</p>
    <p class="font2">Lipsum as it is sometimes</p>
  </div>
</div>
<div class="set_img">
  <div class="row ">
    <?php
    $getPackage = "SELECT  * FROM package";
    $res = mysqli_query($connection, $getPackage);
    while ($packRow = mysqli_fetch_array($res)) { ?>
      <a href="destinationDetail.php?id=<?php echo $packRow['id']; ?>">
        <div class="col-md-4">
          <div class="overflow">
            <div class="layer"></div>
            <img src="admin/../upload/package/<?php echo $packRow['photo']; ?>" width="100%" class="img2">
            <div class="textlayer3">
              <p class="font9"><?php echo ucwords($packRow['title']); ?></p>
              <p class="font10"><?php echo ucwords($packRow['category']); ?></p>
            </div>
          </div>
        </div>
      </a>
    <?php } ?>
  </div>
</div>
<?php
include 'partials/footer.php';
?>