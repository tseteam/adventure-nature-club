<?php
require_once('admin/includes/config.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Home Page </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" />

    <link rel="stylesheet" href="style.css" />
</head>

<body>
    <div class="mainnav">
        <div class="row ">
            <div class="col-md-3">
                <p class="left_nav">ROAM</p>
            </div>
            <div class="col-md-7">
                <span class="middle_nav"><a href="index.php"> Home</a></span>
                <span class="middle_nav"><a href="destinationList.php"> Destinations</a></span>
                <span class="middle_nav">Tours</span>
                <span class="middle_nav">Pages</span>
                <span class="middle_nav">Blog</span>
                <span class="middle_nav">Elements</span>
            </div>

            <div class="col-md-2"><i class="fas fa-bars icon"></i></div>
        </div>
    </div>