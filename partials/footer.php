<div class="row padding2">
    <div class="col-md-3">
        <h1>Roam</h1>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipi. Suspend isse ultrices
            hendrerit nunc vitae vel a sodales. Ac lectus vel risus suscipit
            venenatis.
        </p>
        <p>
            Strömgatan 18, Stockholm, Sweden(+46) 322.170.71 ouroffice@example.com
        </p>
    </div>
    <div class="col-md-3">
        <h1>Links</h1>
        <p><i class="fas fa-dot-circle"></i>Creative style of home</p>
        <p><i class="fas fa-dot-circle"></i>Custom image title and creative</p>
        <p><i class="fas fa-dot-circle"></i>Custom font style and contact</p>
        <p><i class="fas fa-dot-circle"></i>Smooth parallax all around</p>
        <p><i class="fas fa-dot-circle"></i>Crafted beautiful elements</p>
    </div>
    <div class="col-md-3">
        <h1>Twitter Feed</h1>

        <p>
            <i class="fas fa-dove icon2"></i>
            RT @wpklik: Here is our selection of 20 #WordPress themes with Slider
            Revolution plugin you should check out! @edgethemes @selectthemes @mi…
        </p>
        <p>4 days ago</p>
        <p>
            <i class="fas fa-dove icon2"></i>
            Meet Curly! https://t.co/Hiy1pJmnKP Now trending on ThemeForest, this
            #WordPress theme is made for #hair salons an… https://t.co/XNDaTAxXkM
        </p>
        <p>4 days ago</p>
    </div>

    <div class="col-md-3">
        <h1>Instagram Gallery</h1>
        <div class="padding3">
            <div class="row">
                <?php
        $getImage = "SELECT  * FROM gallery order by id desc limit 6";
        $res = mysqli_query($connection, $getImage);
        while ($packRow = mysqli_fetch_array($res)) { ?>
                <div class="col-md-4 column_margin">
                    <img src="admin/../upload/gallery/<?php echo $packRow['image_name']; ?>" width="100%" height="78px">
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <p>Copyright Mikadothems</p>
</div>
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</body>

</html>