<?php
require_once('admin/includes/config.php');
?>
<!DOCTYPE html>
<html>

<head>
    <title>Welcome Dream Tour Side</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="./admin/assets/css/style.css">
</head>

<body>
    <div class="container">
        <!----------------- for showing category ---------------->
        <?php
        $getCat = mysql_query("SELECT * FROM category ");
        while (
            $row = mysql_fetch_array($getCat)
        ) {
            $catName = $row['cate_name'];
            ?>
        <div class="content">
            <h1>
                <?php echo ucwords($catName); ?>
            </h1>
        </div>
        <!----------------- for showing category with there packages ---------------->
        <div class="row">
            <?php
                $getPackage =  mysql_query("SELECT  * FROM package  where category = '$catName' order by id limit 3");
                while ($packRow = mysql_fetch_array($getPackage)) { ?>
            <div class="col-md-4 back">
                <div class="card">
                    <div class="smallcard">
                        <a href="show.php?id=<?php echo $packRow['id']; ?>"><img
                                src="admin/../upload/package/<?php echo $packRow['photo']; ?>" width="100%;"></a>
                        <div class="titlehead">
                            <h3><?php echo ucfirst($packRow['title']); ?></h3>
                            <p><?php echo $packRow['description']; ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="view">
            <a href="showpagecat.php?id=<?php echo $row['cat_id']; ?>" class="btnborder">View More</a>
        </div>
        <?php } ?>
    </div>

</body>

</html>