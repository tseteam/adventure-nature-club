<?php
include 'partials/header.php';
?>
<?php
$getSliderImage = "select * from slider_image order by id desc limit 1";
$res = mysqli_query($connection, $getSliderImage);
while ($row = mysqli_fetch_array($res)) { ?>
    <div class="hero1" style="background-image:url('admin/../upload/slider_image/<?php echo $row['image']; ?>');background-repeat: no-repeat;
  background-size: cover;">
        <div class="transparent_layer">
            <p class="font1"><?php echo $row['title']; ?></p>
            <p class="font2"><?php echo $row['description']; ?></p>
        </div>
    </div>
<?php } ?>
<div class="imgHeight">
    <div class="row ">
        <?php
        $getPackage = "SELECT  * FROM package  where category = 'popular package' order by id limit 4";
        $res = mysqli_query($connection, $getPackage);
        while ($packRow = mysqli_fetch_array($res)) { ?>
            <a href="destinationDetail.php?id=<?php echo $packRow['id']; ?>">
                <div class="col-md-3 column_margin imghover">
                    <img src="admin/../upload/package/<?php echo $packRow['photo']; ?>" width="100%" class="img1">
                    <div class="layer"></div>
                    <div class="textlayer">
                        <div class="row padding">
                            <div class="col-md-7">
                                <p class="font5"><?php echo ucwords($packRow['title']); ?></p>
                                <p class="font6"><?php echo ucwords($packRow['category']); ?></p>
                            </div>
                            <div class="col-md-5">
                                <span><i class="fas fa-rupee-sign color"></i><?php echo ucfirst($packRow['price']); ?></span>
                            </div>
                        </div>
                        <div>
                            <p class="color2">
                                <?php echo $packRow['description']; ?>
                            </p>
                        </div>
                    </div>
                </div>
            </a>
        <?php } ?>
    </div>
</div>
<div class="hero2">
    <div class="transparent_layer2">
        <div class="row">
            <div class="col-md-6">
                <p class="f1">15% off</p>
            </div>
            <div class="col-md-6">
                <p class="f2">
                    Last Minute Offer For You
                </p>
                <p class="f3">
                    Aerial view of Cape Town with Cape Town Stadium
                </p>
                <p>
                    Last Minute Offer For You Aerial view of Cape Town with Cape Town
                    Stadium Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aliquam hendrerit felis sit amet turpis vehicula convallis. Ut ac
                    tellus velit. Nulla mollis sollicitudin lacus id ornare. Phasellus
                    laoreet nulla et nulla sagittis, sit amet cursus urna mollis.
                </p>
                <button type="submit" class="btn1">Buy Theme</button>
            </div>
        </div>
    </div>
</div>

<div class="row ">
    <div class="col-md-6 column_margin">
        <div class="trans">
            <img src="https://images.unsplash.com/photo-1564415144119-43ffa614fe50?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80" width="100%" class="img1" height="541px" alt="" />
        </div>
    </div>
    <div class="col-md-6 column_margin">
        <div class="row">
            <div class="col-md-6 column_margin">
                <img src="https://images.unsplash.com/photo-1564359732441-c879d1a16c49?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80" width="100%" height="100%" class="img1" alt="" />
            </div>
            <div class="col-md-6 column_margin">
                <img src="https://images.unsplash.com/photo-1555232143-3d29b8a084b8?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" width="100%" height="100%" class="img1" alt="" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 column_margin">
                <img src="https://images.unsplash.com/photo-1551127059-b2f01cd9d05e?ixlib=rb-1.2.1&auto=format&fit=crop&w=1039&q=80" width="100%" height="100%" class="img1" alt="" />
            </div>
            <div class="col-md-6 column_margin">
                <img src="https://images.unsplash.com/photo-1551127059-b2f01cd9d05e?ixlib=rb-1.2.1&auto=format&fit=crop&w=1039&q=80" width="100%" height="100%" class="img1" alt="" />
            </div>
        </div>
    </div>
</div>
<div class="hero3">
    <div class="transparent_layer3">
        <p class="font3">Choose a Country For Your Next Adventure?</p>
        <p class="font4">
            We Will Inform You When We Have Found Something Special
        </p>

        <span class="space"><input type="text" placeholder="Travel Type?" class="text" /></span>
        <button type="submit" class="btn">Find Now</button>
    </div>
</div>
<div class="heightimg">
    <div class="row border">
        <?php
        $getPackage = "SELECT  * FROM package  where category = 'winter package' order by id limit 3";
        $res = mysqli_query($connection, $getPackage);
        while ($packRow = mysqli_fetch_array($res)) { ?>
            <a href="destinationDetail.php?id=<?php echo $packRow['id']; ?>">
                <div class="col-md-4 column_margin imghover">
                    <img src="admin/../upload/package/<?php echo $packRow['photo']; ?>" width="100%" class="img1">
                    <div class="layer"></div>
                    <div class="textlayer2">
                        <p class="font7"><?php echo ucwords($packRow['title']); ?></p>
                        <p class="font8"><?php echo ucwords($packRow['category']); ?></p>
                    </div>
                </div>
            </a>
        <?php } ?>
    </div>
</div>
<?php
include 'partials/footer.php';
?>