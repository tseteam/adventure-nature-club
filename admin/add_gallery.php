<?php
require_once('includes/config.php');
?>
<!----------------- insert the session ---------->
<?php
if ($_SESSION['auth'] != NULL &&  $_SESSION['pass'] != NULL) { ?>
<!--- includes the header file-->
<?php require_once('includes/header.php'); ?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Add New Image</h4>
                        <p class="card-category"></p>
                    </div>
                    <div class="card-body">
                        <form method="post" action="addformgallery.php" enctype="multipart/form-data" id="form">
                            <div class="col-md-6">
                                <input type="file" name="photo" id="image">
                            </div>
                            <button type="submit" name="add" class="btn btn-primary pull-right">Add gallery</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--- includes the footer file-->
<?php require_once('includes/footer.php'); ?>
<!-- <script>
    const fd = new FormData();
    var uploadimg = $('#image').prop('files')[0];
    function multipleImage(event) {
        let fileLength = event.target.files.length;
        for (let i = 0; i < fileLength; i++) {
            //console.log(event.target.files[i]);
            let data = event.target.files[i];
            fd.append('image_name', data);
        }
        //console.log(fd);
    }
    $('#form').submit(function() {
        $.ajax({
            url: "addformgallery.php",
            type: "POST",
            // async: false,
            // enctype: "multipart/form-data",
            contentType: false,
            processData: false,
            cache: false,
            data: fd,
            success: function(data) {
                // alert(data);
                console.log(data);
                //window.location.href = "gallery.php";
            },
            error: function(data) {},
        });
    })
</script> -->
<?php  } else {
    header('Location:log_in.php');
}
?>