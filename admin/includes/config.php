<?php
//connection with server
$connection = mysqli_connect('localhost', 'root', '');
if (!$connection) {
    die("Database Connection Failed" . mysqli_error($connection));
}
$select_db = mysqli_select_db($connection, 'tour');
if (!$select_db) {
    die("Database Selection Failed" . mysqli_error($connection));
}
//Init Session
session_name("auth");

// Start Session
session_start();

// Set Common Dateformat according to timezone
date_default_timezone_set("Asia/kolkata");

//Set Project Path / Base URL
define("BASEURL", "http://localhost/adventure-nature-club/admin/log_in.php");

// // Set path for an attachment
// define("AVATAR_DIR",BASEURL."images/");