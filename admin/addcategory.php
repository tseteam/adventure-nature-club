<?php
require_once('includes/config.php');
?>
<!----------------- insert the session ---------->
<?php
if ($_SESSION['auth'] != NULL &&  $_SESSION['pass'] != NULL) { ?>
  <!--- includes the header file-->
  <?php require_once('includes/header.php'); ?>
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">Add New Category</h4>
              <p class="card-category"></p>
            </div>
            <div class="card-body">
              <form method="post" action="addformcate.php" enctype="multipart/form-data">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Category Title</label>
                    <input type="text" name="catename" class="form-control">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Category Description</label>
                    <input type="text" name="catedesc" class="form-control">
                  </div>
                </div>
                <div class="col-md-6">
                  <input type="file" name="photo">
                </div>
                <button type="submit" name="addcate" class="btn btn-primary pull-right">Add category</button>
                <div class="clearfix"></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--- includes the footer file-->
  <?php require_once('includes/footer.php'); ?>
<?php  } else {
  header('Location:log_in.php');
}
?>