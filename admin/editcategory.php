<?php
require_once('includes/config.php');
$ids = $_GET['cat_id'];
//print_r($ids);exit();
//selecting the data from dtabase using id
$proddetails = "select * from category where cat_id='$ids'";
$res = mysqli_query($connection, $proddetails);
$row = mysqli_fetch_array($res);
?>
<!--------- insert session ---------->
<?php
if ($_SESSION['auth'] != NULL &&  $_SESSION['pass'] != NULL) { ?>
  <!--- includes the header file-->
  <?php require_once('includes/header.php'); ?>
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">Edit Category</h4>
              <p class="card-category"></p>
            </div>
            <div class="card-body">
              <form method="post" action="editformcate.php" enctype="multipart/form-data">
                <!------------- for title ------------------>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Category Title</label>
                    <input type="text" name="cate_name" class="form-control" value="<?php echo $row['cate_name']; ?>">
                  </div>
                </div>
                <!------------- for description ------------------>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Category Description</label>
                    <input type="text" name="cate_desc" class="form-control" value="<?php echo $row['cate_desc']; ?>">
                  </div>
                </div>
                <!------------- for photo ------------------>
                <div class="col-md-12">
                  <img src="../upload/category/<?php echo $row['photo']; ?>" alt="image" width="50px" />
                  <input type="file" name="photo">
                  <input type="hidden" name="old_image" value="<?php echo $row['photo']; ?>">
                  <input type="hidden" name="old_id" value="<?php echo $row['cat_id']; ?>">
                </div>
                <button type="submit" name="edit" class="btn btn-primary pull-right">save edit</button></td>
                <div class="clearfix"></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--- includes the footer file-->
  <?php require_once('includes/footer.php'); ?>
<?php  } else {
  header('Location:log_in.php');
}
?>