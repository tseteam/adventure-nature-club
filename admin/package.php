<?php
require_once('includes/config.php');
//for showing the all product details
$getallproducts = "select * from package";
$res = mysqli_query($connection, $getallproducts);
?>
<!----------- insert the session -------------->
<?php
if ($_SESSION['auth'] != NULL &&  $_SESSION['pass'] != NULL) { ?>
  <!--- includes the header file-->
  <?php require_once('includes/header.php'); ?>
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Packages</h4>
              <p class="card-category"> Here is Lists of Packages</p>
              <button type="button" class="btn btn-primary pull-right" onclick="window.location='addpackage.php'">add package</button>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>Title</th>
                    <th>Photo</th>
                    <th>Category</th>
                    <th>Action</th>
                  </thead>
                  <tbody>
                    <!-- showing the list of blog details -->
                    <?php
                      while ($row = mysqli_fetch_array($res)) { ?>
                      <tr>
                        <td><?php echo $row['title']; ?></td>
                        <td><img src="../upload/package/<?php echo $row['photo']; ?>" alt="image" width="50px" /></td>
                        <td><?php echo $row['category']; ?></td>
                        <td><a href="editpackage.php?id=<?php echo $row['id']; ?>"><i class="material-icons">edit</i></a>
                          <a href="deletepackage.php?id=<?php echo $row['id']; ?>"><i class="material-icons">delete</i></a></td>
                        </form>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--- includes the footer file-->
  <?php require_once('includes/footer.php'); ?>
<?php  } else {
  header('Location:log_in.php');
}
?>