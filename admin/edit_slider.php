<?php
require_once('includes/config.php');
$ids = $_GET['id'];
//print_r($ids);exit();
//selecting the data from dtabase using id
$sliderData = "select * from slider_image where id='$ids'";
$res = mysqli_query($connection, $sliderData);
$row = mysqli_fetch_array($res);
?>
<!--------- insert session ---------->
<?php
if ($_SESSION['auth'] != NULL &&  $_SESSION['pass'] != NULL) { ?>
    <!--- includes the header file-->
    <?php require_once('includes/header.php'); ?>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Edit Category</h4>
                            <p class="card-category"></p>
                        </div>
                        <div class="card-body">
                            <form method="post" action="edit_form_slider.php" enctype="multipart/form-data">
                                <!------------- for title ------------------>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Title</label>
                                        <input type="text" name="title" class="form-control" value="<?php echo $row['title']; ?>">
                                    </div>
                                </div>
                                <!------------- for description ------------------>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Description</label>
                                        <input type="text" name="description" class="form-control" value="<?php echo $row['description']; ?>">
                                    </div>
                                </div>
                                <!------------- for photo ------------------>
                                <div class="col-md-12">
                                    <img src="../upload/slider_image/<?php echo $row['image']; ?>" alt="image" width="50px" />
                                    <input type="file" name="image">
                                    <input type="hidden" name="old_image" value="<?php echo $row['image']; ?>">
                                    <input type="hidden" name="old_id" value="<?php echo $row['id']; ?>">
                                </div>
                                <button type="submit" name="edit" class="btn btn-primary pull-right">save edit</button></td>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--- includes the footer file-->
    <?php require_once('includes/footer.php'); ?>
<?php  } else {
    header('Location:log_in.php');
}
?>