<?php
require_once('includes/config.php');
$getSliderImage = "select * from slider_image";
$res = mysqli_query($connection, $getSliderImage);
?>
<!----------- insert the session -------------->
<?php
if ($_SESSION['auth'] != NULL &&  $_SESSION['pass'] != NULL) { ?>
    <!--- includes the header file-->
    <?php require_once('includes/header.php'); ?>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Slider Gallery</h4>
                            <button type="button" class="btn btn-primary pull-right" onclick="window.location='add_slider_image.php'">Add Image</button>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Photo</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                                        <!-- showing the list of blog details -->
                                        <?php
                                            while ($row = mysqli_fetch_array($res)) { ?>
                                            <tr>
                                                <td><?php echo $row['title']; ?></td>
                                                <td><?php echo $row['description']; ?></td>
                                                <td><img src="../upload/slider_image/<?php echo $row['image']; ?>" alt="image" width="50px" height="50px" /></td>
                                                <td><a href="edit_slider.php?id=<?php echo $row['id']; ?>"><i class="material-icons">edit</i></a>
                                                    <a href="delete_slider.php?id=<?php echo $row['id']; ?>"><i class="material-icons">delete</i></a></td>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--- includes the footer file-->
    <?php require_once('includes/footer.php'); ?>
<?php  } else {
    header('Location:log_in.php');
}
?>