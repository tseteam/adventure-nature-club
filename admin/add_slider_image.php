<?php
require_once('includes/config.php');
?>
<!----------------- insert the session ---------->
<?php
if ($_SESSION['auth'] != NULL &&  $_SESSION['pass'] != NULL) { ?>
<!--- includes the header file-->
<?php require_once('includes/header.php'); ?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Add New Image</h4>
                        <p class="card-category"></p>
                    </div>
                    <div class="card-body">
                        <form method="post" action="addFormSlider.php" enctype="multipart/form-data" id="form">
                        <!------------- for title ------------------>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label class="bmd-label-floating">Title</label>
                                <input type="text" name="title" id="title" class="form-control">
                                </div>
                            </div>
                            <!------------- for description ------------------>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label class="bmd-label-floating">Description</label>
                                <input type="text" name="description" id="description" class="form-control">
                                </div>
                            </div>
                              <!------------- for image ------------------>
                            <div class="col-md-6">
                                <input type="file" name="photo">
                            </div>
                            <button type="submit" name="addImage" class="btn btn-primary pull-right">Add Image</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--- includes the footer file-->
<?php require_once('includes/footer.php'); ?>
<?php  } else {
    header('Location:log_in.php');
}
?>