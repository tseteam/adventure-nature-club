<?php
require_once('includes/config.php');
$get_all_images = "select * from gallery";
$res = mysqli_query($connection, $get_all_images);
?>
<!----------- insert the session -------------->
<?php
if ($_SESSION['auth'] != NULL &&  $_SESSION['pass'] != NULL) { ?>
    <!--- includes the header file-->
    <?php require_once('includes/header.php'); ?>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Gallery</h4>
                        </div>
                        <div class="card-body">
                            <?php
                                while ($row = mysqli_fetch_array($res)) { ?>
                                <span><img src="../upload/gallery/<?php echo $row['image_name']; ?>" alt="image" width="50px" height="50px" /></span>
                            <?php } ?>
                            <button type="button" class="btn btn-primary pull-right" onclick="window.location='add_gallery.php'">add gallery</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--- includes the footer file-->
    <?php require_once('includes/footer.php'); ?>
<?php  } else {
    header('Location:log_in.php');
}
?>