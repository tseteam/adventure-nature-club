<?php
require_once('includes/config.php');
?>
<!--------- insert session ---------->
<?php
if ($_SESSION['auth'] != NULL &&  $_SESSION['pass'] != NULL) { ?>
  <!--- includes the header file-->
  <?php require_once('includes/header.php'); ?>
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">Add New Package</h4>
            </div>
            <div class="card-body">
              <form method="post" id="formdata" enctype="multipart/form-data">
                <!------------- for title ------------------>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Title</label>
                    <input type="text" name="title" id="title" class="form-control">
                  </div>
                </div>
                <!------------- for description ------------------>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Description</label>
                    <input type="text" name="description" id="description" class="form-control">
                  </div>
                </div>
                <!------------- for duration ------------------>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Duration</label>
                    <input type="text" name="duration" id="duration" class="form-control">
                  </div>
                </div>
                <!------------- for price ------------------>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Price</label>
                    <input type="text" name="price" id="price" class="form-control">
                  </div>
                </div>
                <!------------- for location ------------------>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Location</label>
                    <input type="text" name="location" id="location" class="form-control">
                  </div>
                </div>
                <!------------- for category ------------------>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Category</label>
                    <select type="text" name="category" id="category" class="form-control">
                      <?php $s = "select cate_name from category";
                        $res = mysqli_query($connection, $s);
                        while ($rw = mysqli_fetch_array($res)) { ?>
                        <option value="<?php echo $rw['cate_name']; ?>"><?php echo $rw['cate_name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <!------------- for photo ------------------>

                <div class="col-md-6">
                  <input type="file" name="photoName" id="photoName">
                </div>
                <br>
                <!-------------- for editor ---------------->
                <div id="editor"></div>
                <!-- <div id="output"></div> -->
                <!------------- for button ------------------>
                <button type="submit" name="add" id="submit" class="btn btn-primary pull-right">Add Package</button>
                <div class="clearfix"></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--- includes the footer file-->
  <?php require_once('includes/footer.php'); ?>
  <script>
    var imageName;
    var ext;
    var imgFullName;

    $('#photoName').change(function() {
      imageName = this.files[0].name.split('.')[0];
      ext = this.files[0].name.split('.')[1];
      imgFullName = imageName + '.' + ext;
    })

    $('#formdata').on('submit', function(event) {
      event.preventDefault()
      var title = $('#title').val();
      var description = $('#description').val();
      var duration = $('#duration').val();
      var price = $('#price').val();
      var location = $('#location').val();
      var category = $('#category').val();
      var files = $('#photoName')[0];
      //let uploadimg = document.getElementById('photoName').files[0];
      var data = new FormData();
      var uploadimg = $('#photoName').prop('files')[0];
      data.append('title', title);
      data.append('description', description);
      data.append('duration', duration);
      data.append('price', price);
      data.append('location', location);
      data.append('category', category)
      data.append('photoName', uploadimg);
      //console.log(files);
      //console.log(data);
      var delta = JSON.stringify(quill.getText());
      data.append('itinerary', delta);
      //console.log(delta);

      $.ajax({
        url: "addpackform.php",
        type: "POST",
        // async: false,
        // enctype: "multipart/form-data",
        contentType: false,
        processData: false,
        cache: false,
        data: data,
        success: function(data) {
          // alert(data);
          //console.log(data);
          window.location.href = "package.php";
        },
        error: function(data) {

        },
      });
      //console.log(data);
      //console.log(title, description, duration, price, location, category, photo)
    });
  </script>
<?php  } else {
  header('Location:log_in.php');
}
?>