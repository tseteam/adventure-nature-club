<?php
require_once('includes/config.php');
//for showing the all product details
$getallcat = "select * from category";
$res = mysqli_query($connection, $getallcat);
?>
<!----------- insert the session -------------->
<?php
if ($_SESSION['auth'] != NULL &&  $_SESSION['pass'] != NULL) { ?>
  <!--- includes the header file-->
  <?php require_once('includes/header.php'); ?>
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Categories</h4>
              <p class="card-category"> Here is Lists of Category</p>
            </div>
            <form method="post">
              <div class="card-body">
                <button type="button" class="btn btn-primary pull-right" onclick="window.location='addcategory.php'">add categories</button>
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>Category name</th>
                      <th>Category Description</th>
                      <th>photo</th>
                      <th>Action</th>
                    </thead>
                    <tbody>
                      <!-- showing the list of product details -->
                      <?php
                        while ($row = mysqli_fetch_array($res)) { ?>
                        <tr>
                          <td><?php echo $row['cate_name']; ?></td>
                          <td><?php echo $row['cate_desc']; ?></td>
                          <td><img src="../upload/category/<?php echo $row['photo']; ?>" alt="image" width="50px" /></td>
                          <td><a href="editcategory.php?cat_id=<?php echo $row['cat_id']; ?>">Edit</a>
                            <a href="deletecategory.php?cat_id=<?php echo $row['cat_id']; ?>">Delete</a></td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </form>

  <!--- includes the footer file-->
  <?php require_once('includes/footer.php'); ?>
<?php  } else {
  header('Location:log_in.php');
}
?>