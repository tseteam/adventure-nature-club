<?php
require_once('admin/includes/config.php');
//for showing the all product details
$ids = $_GET['id'];
$getpackage = mysql_query("select * from package where id= '$ids'");
?>
<!DOCTYPE html>
<html>

<head>
  <title>Show Details of packages</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="./admin/assets/css/style.css">
</head>

<body>
  <div class="container">
    <div class="row">
      <?php
      while ($row = mysql_fetch_array($getpackage)) { ?>
        <div class="col-md-12">
          <div class="card">
            <div class="bigcard">
              <img src="admin/../upload/package/<?php echo $row['photo']; ?>" width="100%">
              <div class="titlehead">
                <h3>Title: <?php echo ucfirst($row['title']); ?></h3>
                <p><b>Description: </b><?php echo $row['description']; ?></p>
                <h4>Duration: <?php echo ucfirst($row['duration']); ?></h4>
                <h4>Price: <?php echo ucfirst($row['price']); ?></h4>
                <h4>Location: <?php echo ucfirst($row['location']); ?></h4>
                <h4>Category: <?php echo ucfirst($row['category']); ?></h4>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</body>

</html>