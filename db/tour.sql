-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 30, 2019 at 11:48 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tour`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

DROP TABLE IF EXISTS `admin_login`;
CREATE TABLE IF NOT EXISTS `admin_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` timestamp NOT NULL,
  `modified` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`id`, `email`, `password`, `created`, `modified`) VALUES
(1, 'aa@gmail.com', 'aa123', '2019-08-29 13:49:56', '2019-08-29 13:49:56');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(255) NOT NULL,
  `cate_desc` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `created` timestamp NOT NULL,
  `modified` timestamp NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `cate_name`, `cate_desc`, `photo`, `created`, `modified`) VALUES
(8, 'winter package', 'Lorem Ipsum is simply dummy text of the printing', '5d68db05d25591.27557854.jpg', '2019-08-30 08:15:01', '2019-08-30 08:15:01'),
(7, 'summer package', 'Lorem Ipsum is simply dummy text of the printing', '5d68dacce0eb80.27477734.jpg', '2019-08-30 08:06:22', '2019-08-30 08:06:22'),
(6, 'popular package', 'Lorem Ipsum is simply dummy text of the printing', '5d68d8ceb9d051.76291155.jpg', '2019-08-30 08:05:34', '2019-08-30 08:05:34'),
(10, 'monsoon package', 'Lorem Ipsum is simply dummy text of the printing', '5d68dbb0e07111.85207363.jpg', '2019-08-30 08:17:52', '2019-08-30 08:17:52'),
(11, 'special summer package', 'Lorem Ipsum is simply dummy text of the printing', '5d68df5de29885.70265116.jpg', '2019-08-30 08:33:33', '2019-08-30 08:33:33');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_name` varchar(255) NOT NULL,
  `created` timestamp NOT NULL,
  `modified` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `image_name`, `created`, `modified`) VALUES
(1, '5d68ed63af6839.05819808.jpg', '2019-08-30 09:33:23', '2019-08-30 09:33:23'),
(2, '5d68ed6aad1034.29575014.jpg', '2019-08-30 09:33:30', '2019-08-30 09:33:30'),
(3, '5d68ed71b27ba1.48406234.jpg', '2019-08-30 09:33:37', '2019-08-30 09:33:37'),
(4, '5d68ed79df4660.45487922.jpg', '2019-08-30 09:33:45', '2019-08-30 09:33:45');

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

DROP TABLE IF EXISTS `package`;
CREATE TABLE IF NOT EXISTS `package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `price` bigint(20) NOT NULL,
  `location` varchar(255) NOT NULL,
  `itinerary` varchar(255) NOT NULL,
  `created` timestamp NOT NULL,
  `modified` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `package`
--

INSERT INTO `package` (`id`, `title`, `description`, `photo`, `category`, `duration`, `price`, `location`, `itinerary`, `created`, `modified`) VALUES
(4, 'manali holidays', 'Lorem Ipsum is simply dummy text of the printing.', '5d68eb7fcc8b57.54838852.jpg', 'winter package', '3 months', 150000, 'manali', '\"Where does it come from\nContrary to popular belief, Lorem Ipsum is not simply random text.\n It has roots in a piece of classical Latin literature.\n\n\"', '2019-08-30 09:08:38', '2019-08-30 09:08:38'),
(3, 'ooty holidays', ' software like Aldus PageMaker including versions of Lorem Ipsum.', '5d68e75245e956.54614438.jpg', 'winter package', '6 months', 200000, 'ooty', '\"What is Lorem Ipsum\nLorem IpsumÂ is simply dummy text .\nthe printing and typesetting industry.\n\n\"', '2019-08-30 09:07:30', '2019-08-30 09:07:30'),
(12, 'goa holidays', ' software like Aldus PageMaker including versions of Lorem Ipsum.', '5d68ec29778d49.38791539.jpg', 'monsoon package', '6 months', 200000, 'goa', '\"What is Lorem Ipsum\nLorem IpsumÂ is simply dummy text.\nThe printing and typesetting industry.\n\n\"', '2019-08-30 09:28:09', '2019-08-30 09:28:09'),
(6, 'munnar package', 'Lorem Ipsum is simply ', '5d68e82b4f5077.91280161.jpg', 'popular package', '3 night', 300000, 'munnar', '\"Why do we use it\nIt is a long established fact that a reader will be distracted.\nThe readable content of a page when looking at its layout.\n\n\"', '2019-08-30 09:11:07', '2019-08-30 09:11:07'),
(7, 'jaipur holiday', ' software like Aldus PageMaker including versions of Lorem Ipsum.', '5d68e8608e6151.11540490.jpg', 'popular package', '6 months', 200000, 'jaipur', '\"Why do we use it\nIt is a long established fact that a reader will be distracted.\nThe readable content of a page when looking at its layout.\n\n\"', '2019-08-30 09:12:00', '2019-08-30 09:12:00'),
(8, 'kashmir holidays', ' software like Aldus PageMaker including versions of Lorem Ipsum.', '5d690c033d2cc0.17789791.jpg', 'special summer package', '2 months', 250000, 'kashmir', '\"Why do we use it\nIt is a long established fact that a reader will be distracted.\nThe readable content of a page when looking at its layout.\n\n\"', '2019-08-30 09:13:31', '2019-08-30 09:13:31'),
(9, 'singapore holidays', 'Lorem Ipsum is simply ', '5d68eb9cd8b797.43682279.jpg', 'winter package', '5 nights', 300000, 'singapore', '\"Why do we use it\nIt is a long established fact that a reader will be distracted.\nThe readable content of a page when looking at its layout.\n\"', '2019-08-30 09:14:07', '2019-08-30 09:14:07'),
(10, 'Darjeeling holidays', 'Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.', '5d68e927bd4a75.36287349.jpg', 'popular package', '2 months', 150000, 'Darjeeling ', '\"Where can I get some\nThere are many variations of passages of Lorem Ipsum available.\n The majority have suffered alteration in some form,Â \n\n\"', '2019-08-30 09:15:19', '2019-08-30 09:15:19'),
(11, 'delhi package', ' software like Aldus PageMaker including versions of Lorem Ipsum.', '5d68e9e838beb4.27297638.jpg', 'popular package', '2 months', 200000, 'delhi', '\"Where can I get some\nThere are many variations of passages of Lorem Ipsum available.\nThe majority have suffered alteration in some form,Â \n\n\"', '2019-08-30 09:18:32', '2019-08-30 09:18:32');

-- --------------------------------------------------------

--
-- Table structure for table `slider_image`
--

DROP TABLE IF EXISTS `slider_image`;
CREATE TABLE IF NOT EXISTS `slider_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created` timestamp NOT NULL,
  `modified` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider_image`
--

INSERT INTO `slider_image` (`id`, `title`, `description`, `image`, `created`, `modified`) VALUES
(3, 'Lorem Ipsum', 'versions of Lorem Ipsum', '5d68f1b22920a2.50548666.jpg', '2019-08-30 09:47:23', '2019-08-30 09:47:23'),
(2, ' release ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '5d68f043b3bb00.92918584.jpg', '2019-08-30 09:38:35', '2019-08-30 09:38:35');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
