<?php
require_once('admin/includes/config.php');
$ids = $_GET['id'];
//echo $ids;
$getpackage = "select * from package where id= '$ids'";
$res = mysqli_query($connection, $getpackage);
// $getGallery = mysql_query('select * from gallery');
// $rows = mysql_fetch_array($getGallery);
?>
<?php
include 'partials/header.php';
?>
<?php
while ($row = mysqli_fetch_array($res)) { ?>

  <div class="bgimg" style="background-image:url('admin/../upload/package/<?php echo $row['photo']; ?>'); background-repeat: no-repeat;
  background-size: cover;">
    <div class="layer"></div>
    <div class="imgData">
      <h1 class="hike mar">
        <b><?php echo ucwords($row['title']); ?></b>
      </h1>
      <img src="https://roam.mikado-themes.com/wp-content/plugins/mikado-tours/assets/img/separator.png">
    </div>
  </div>

  <ul class="nav nav-tabs">
    <li class="active nav2"><a data-toggle="tab" href="#info" class="tabpad"><i class="far fa-file-alt includepad"></i>Information</a></li>
    <li class="nav2"><a data-toggle="tab" href="#tour" class="tabpad"><i class="fas fa-book-open includepad"></i>Tour Plan </a></li>
    <li class="nav2"><a data-toggle="tab" href="#location" class="tabpad"><i class="fas fa-map-marker-alt includepad"></i>Location</a></li>
    <li class="nav2"><a data-toggle="tab" href="#gallery" class="tabpad"><i class="fas fa-camera includepad"></i>Gallery</a></li>
    <!-- <li class="nav2"><a data-toggle="tab" href="#reviews" class="tabpad"><i class="fas fa-user-friends includepad"></i>Reviews</a></li> -->
  </ul>
  <div class="tab-content">
    <div id="info" class="tab-pane fade in active">
      <div class="container">
        <div class="col-sm-8">
          <h3 class="hiking"><?php echo ucwords($row['title']); ?></h3>
          <h4 class="h4cls" style="color: tomato;"><i class="fas fa-rupee-sign iconfont"></i><?php echo ucwords($row['price']); ?>/ per person</h4>
          <p class="h4cls"><?php echo ucwords($row['description']); ?></p>

          <h4 class="tab tabbpadd h4cls"><i class="far fa-clock iconfont"><?php echo ucwords($row['duration']); ?></i></h4>
          <h4 class="tab tabbpadd h4cls"><i class="fas fa-pager iconfont"></i>23+ Age</h4>
          <h4 class="tab tabbpadd h4cls"><i class="fas fa-heart iconfont"></i>Destination</h4>
          <hr>
          <div class="row">
            <div class="col-sm-3">
              <h4 class="tab"><b>Destination</b></h4>
            </div>
            <div class="col-sm-9">
              <h5 class="tab leftpad"><?php echo ucwords($row['category']); ?></h5><br>
              <hr>
            </div>
            <div class="col-sm-3">
              <h4 class="tab"><b>Departure</b> </h4>
            </div>
            <div class="col-sm-9">
              <h5 class="tab leftpad"> Please arrive by 9:15 AM for a prompt departure at 9:30 AM.</h5><br>
              <hr>
            </div>
            <div class="col-sm-3">
              <h4 class="tab"><b>Departure Time </b> </h4>
            </div>
            <div class="col-sm-9">
              <h5 class="tab leftpad"> Approximately 8:30 PM.</h5><br>
              <hr>
            </div>
            <div class="col-sm-3">
              <h4 class="tab"><b>Dress Code</b></h4>
            </div>
            <div class="col-sm-9">
              <h5 class="tab leftpad">Casual, Comfortable athletic clothing, hiking shoes, hat and warm jacket.</h5><br>
              <hr>
            </div>
            <div class="col-sm-3">
              <h4 class="tab"><b>Included</b></h4>
            </div>
            <div class="col-sm-9">
              <h5 class="tab leftpad"><i class="fas fa-check includepad"></i> Personal Guide <i class="fas fa-check includepad"></i>Transportation/Car <br><i class="fas fa-check includepad margleft "></i> Typical Souvenir</h5><br>
              <hr>
            </div>
            <div class="col-sm-3">
              <h4 class="tab"><b>Not Included</b></h4>
            </div>
            <div class="col-sm-9">
              <h5 class="tab leftpad"><i class="fas fa-times includepad"></i>Accommondation <i class="fas fa-times includepad"></i>All Museum Tickets <br> <i class="fas fa-times includepad margleft"></i>Meals</h5>
              <hr>
            </div>
          </div>
          <h1 class="hiking">From Our Gallery</h1>
          <h4 class="h4cls" style="color: tomato;"><i>Be our guest and see it for your self</i></h4>
          <h4 class="h4cls">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sollicitudin, tellus vitae condimentum egestas, libero dolor auctor tellus, eu consectetur neque elit quis nunc. Cras elementum pretium est.</h4>
          <div class="row">
            <?php $getGallery = 'select * from gallery order by id desc limit 3';
              $res = mysqli_query($connection, $getGallery);
              while ($rows = mysqli_fetch_array($res)) { ?>
              <div class="col-sm-4">
                <img class="img" src="admin/../upload/gallery/<?php echo $rows['image_name']; ?>" width="100%" height="340px">
              </div>
            <?php } ?>
          </div>
        </div>

        <div class="col-sm-4">
          <img class="rightimgpad" src="admin/../upload/package/<?php echo $row['photo']; ?>" width="100%">
        </div>
      </div>
    </div>


    <div id="tour" class="tab-pane fade">
      <div class="container">
        <div class="col-sm-8">
          <h3 class="h3style"><i class="fas fa-circle includepad"></i><b>Day 1: Departure</b></h3>
          <h4 class="h4style"><?php echo ucwords($row['itinerary']); ?>
          </h4>
          <h4 class="h4style" style="padding-top:15px; ">Donec ultricies, turpis a sagittis suscipit, ex odio volutpat sem, vel molestie varius est.</h4>
          <h4 class="h4style">Suspendisse ultrices nulla eu volutpat volutpat. Proin gravida nibh vel velit auctor aliqueenean.</h4>
          <h4 class="h4style">Nunc tincidunt mollis felis, sed bibendum ligula auctor et. Etiam a erat sit amet augue tincidunt euismod.</h4>
          <h3 class="h3style"><i class="fas fa-circle includepad"></i><b>Day 2:Adventure Beggins</b></h3>
          <h4 class="h4style"><i class="fas fa-check includepad"></i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec viverra purus vitae. </h4>
          <h4 class="h4style"><i class="fas fa-check includepad"></i>Suspendisse ultrices nulla eu volutpat volutpat. Proin gravida nibh vel velit auctor.</h4>
          <h4 class="h4style"><i class="fas fa-check includepad"></i>Nunc tincidunt mollis felis, sed bibendum ligula auctor et. Etiam a erat sit amet augue.</h4>
          <h4 class="h4style"><i class="fas fa-check includepad"></i> Proin gravida nibh vel velit auctor aliqueenean sollicitudin, lorem quis bibendum .</h4>
          <h3 class="h3style"><i class="fas fa-circle includepad"></i><b>Day 8: Historical Tour</b></h3>
          <h4 class="h4style">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec viverra purus vitae venenatis laoreet. Phasellus tincidunt rhoncus rutrum. Mauris a eleifend nisl. Cras ac bibendum massa. Maecenas et pellentesque dui, vitae varius enim.</h4>
          <h3 class="h3style"><i class="fas fa-circle includepad"></i><b>Day 10: Historical Tour</b></h3>
          <h4 class="h4style">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec viverra purus vitae venenatis laoreet. Phasellus tincidunt rhoncus rutrum. Mauris a eleifend nisl.</h4>
          <h4 class="h4style"><i class="fas fa-check includepad"></i>Suspendisse ultrices nulla eu volutpat volutpat. Proin gravida nibh vel velit auctor.</h4>
          <h4 class="h4style"><i class="fas fa-check includepad"></i>Nunc tincidunt mollis felis, sed bibendum ligula auctor et. Etiam a erat sit amet augue.</h4>
          <h3 class="h3style"><i class="fas fa-circle includepad"></i><b>Day 15: Return</b></h3>
          <h4 class="h4style">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec viverra purus vitae venenatis laoreet. Phasellus tincidunt rhoncus rutrum. Mauris a eleifend nisl.</h4>
          <h4 class="h4style"><i class="fas fa-check includepad"></i>Suspendisse ultrices nulla eu volutpat volutpat. Proin gravida nibh vel velit auctor.</h4>
          <h4 class="h4style"><i class="fas fa-check includepad"></i>Nunc tincidunt mollis felis, sed bibendum ligula auctor et. Etiam a erat sit amet augue.</h4>
        </div>
        <div class="col-sm-4">
          <img class="rightimgpad" src="admin/../upload/package/<?php echo $row['photo']; ?>" width="80%" height="450px">
        </div>
      </div>
    </div>
    <div id="location" class="tab-pane fade">
      <div class="container">
        <div class="col-sm-8">
          <img class="imgmargin" src="https://images.unsplash.com/45/eDLHCtzRR0yfFtU0BQar_sylwiabartyzel_themap.jpg?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=757&q=80" width="100%">

          <h3 class="hiking"><?php echo ucwords($row['category']); ?></h3>
          <h4 class="h4style" style="color: tomato;"><i><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sollicitudin, tellus vitae condimentum egestas, libero dolor auctor tellus, eu consectetur neque elit quis nunc. Cras elementum pretium est.</b></i>
          </h4>
          <h4 class="h4style" style="padding-top: 28px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec viverra purus vitae venenatis laoreet. Phasellus tincidunt rhoncus rutrum. Mauris a eleifend nisl. Cras ac bibendum massa. Maecenas et pellentesque dui, vitae varius enim. Ut sit amet turpis lacus. Suspendisse interdum nunc et enim congue fermentum. Aliquam a turpis ac orci pulvinar venenatis. Ut tincidunt ante ex, tempus lobortis leo luctus non. Nulla mattis libero eu tincidunt fermentum. Vestibulum iaculis ac leo et dictum. Etiam porta faucibus orci, in auctor magna vehicula eu. Proin aliquam accumsan est. Sed mattis fringilla eros, ut viverra leo rhoncus id. Mauris vestibulum venenatis diam id pretium. Duis eu est efficitur, egestas nisl non, finibus arcu. Proin quis lorem in purus.
            <h4 class="h4style" style="padding-top:20px; ">Sad naeos, mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim. Suspendisse in orci enim. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</h4>

          </h4>
        </div>
        <div class="col-sm-4"><img class="rightimgpad" src="admin/../upload/package/<?php echo $row['photo']; ?>" width="80%" height="450px"></div>

      </div>
    </div>
  <?php } ?>
  <div id="gallery" class="tab-pane fade">
    <div class="container">
      <div class=" imgmargin imgWrapper">

        <?php $getGallery = 'select * from gallery';
        $res = mysqli_query($connection, $getGallery);
        while ($row = mysqli_fetch_array($res)) { ?>
          <img src="admin/../upload/gallery/<?php echo $row['image_name']; ?>" width="100%">
        <?php } ?>
      </div>
    </div>
  </div>
  </div>
  <?php
  include 'partials/footer.php';
  ?>