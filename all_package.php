<?php
require_once('admin/includes/config.php');
?>
<!DOCTYPE html>
<html>

<head>
    <title>Dream Tour All Packages </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="./admin/assets/css/style.css">
</head>

<body>

    <div class="content fixed">
        <h1>All Packages</h1>
    </div>
    <div class="container">
        <!----------------- for showing category with there packages ---------------->
        <div class="row topmarg">
            <?php
            $getPackage =  mysql_query("SELECT  * FROM package");
            while ($packRow = mysql_fetch_array($getPackage)) { ?>
                <div class="col-md-4 back">
                    <div class="card">
                        <div class="smallcard">
                            <a href="show.php?id=<?php echo $packRow['id']; ?>"><img src="admin/../upload/package/<?php echo $packRow['photo']; ?>" width="100%;"></a>
                            <div class="titlehead">
                                <h3>Title: <?php echo ucfirst($packRow['title']); ?></h3>
                                <p><b>Description: </b><?php echo $packRow['description']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</body>

</html>